const wallet = {
    userName: "Bairaktar",
    bitcoin: {
        nameCoin: "Bitcoin",
        logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/9023.png' alt='Bitcoin' />",
        price: 39817.01,
        coins: 3
    },

    show: function () {
        document.write("<p>" + wallet.bitcoin.nameCoin + "</p>")
        document.write("<p>" + wallet.bitcoin.logo + "</p>")
        document.write("<p>" + wallet.bitcoin.price + "</p>")
    },
    countBit: function () {
        let grnbit = 1182920.00;
        let countB = wallet.bitcoin.coins * grnbit;
        return countB;
    },

    ethereum: {
        nameCoin: "Ethereum",
        logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png' alt='Ethereum' />",
        price: 2955.63,
        coins: 2
    },

    showEthereum: function () {
        document.write("<p>" + wallet.ethereum.nameCoin + "</p>")
        document.write("<p>" + wallet.ethereum.logo + "</p>")
        document.write("<p>" + wallet.ethereum.price + "</p>")
    },

    countEth: function () {
        let grnEth = 86437.63;
        let countE = wallet.ethereum.coins * grnEth;
        return countE;
    },

    stellar: {
        nameCoin: "Stellar",
        logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/512.png' alt='Stellar' />",
        price: 0.1861,
        coins: 50
    },
    showStellar: function () {
        document.write("<p>" + wallet.stellar.nameCoin + "</p>")
        document.write("<p>" + wallet.stellar.logo + "</p>")
        document.write("<p>" + wallet.stellar.price + "</p>")
    },

    countSte: function () {
        let grnSte = 5.50;
        let countS = wallet.stellar.coins * grnSte;
        return countS;
    },

    inputArea: function (one, two, three) {
        let inputCrypt = prompt("Please input the name of the cryptocurrency:");
        if (inputCrypt == this.bitcoin.nameCoin) {
            document.write("Good morning, " + this.userName + "!" + " In your balance left: " + this.bitcoin.coins + " coins, " + "If you sell today you will have " + wallet.countBit() + " grn" + "<br/>") + wallet.show();
        } else if (inputCrypt == this.ethereum.nameCoin) {
            document.write("Good morning, " + this.userName + "!" + " In your balance left: " + this.ethereum.coins + " coins" + "If you sell today you will have " + wallet.countEth() + " grn" + "<br/>") + wallet.showEthereum();
        } else if (inputCrypt == this.stellar.nameCoin) {
            document.write("Good morning, " + this.userName + "!" + " In your balance left: " + this.stellar.coins + " coins" + "If you sell today you will have " + wallet.countSte() + " grn" + "<br/>") + wallet.showStellar();
        }
    }
}

wallet.inputArea()